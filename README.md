# Readme

   ______              ______    __                                       
 /      \            /      \  /  |                                      
/$$$$$$  |  ______  /$$$$$$  |_$$ |_    __   __   __   ______    ______  
$$ \__$$/  /      \ $$ |_ $$// $$   |  /  | /  | /  | /      \  /      \ 
$$      \ /$$$$$$  |$$   |   $$$$$$/   $$ | $$ | $$ | $$$$$$  |/$$$$$$  |
 $$$$$$  |$$ |  $$ |$$$$/      $$ | __ $$ | $$ | $$ | /    $$ |$$ |  $$/ 
/  \__$$ |$$ \__$$ |$$ |       $$ |/  |$$ \_$$ \_$$ |/$$$$$$$ |$$ |      
$$    $$/ $$    $$/ $$ |       $$  $$/ $$   $$   $$/ $$    $$ |$$ |      
 $$$$$$/   $$$$$$/  $$/         $$$$/   $$$$$/$$$$/   $$$$$$$/ $$/       
 _______                                                   __            
/       \                                                 /  |           
$$$$$$$  | ______    ______      __   ______    _______  _$$ |_          
$$ |__$$ |/      \  /      \    /  | /      \  /       |/ $$   |         
$$    $$//$$$$$$  |/$$$$$$  |   $$/ /$$$$$$  |/$$$$$$$/ $$$$$$/          
$$$$$$$/ $$ |  $$/ $$ |  $$ |   /  |$$    $$ |$$ |        $$ | __        
$$ |     $$ |      $$ \__$$ |   $$ |$$$$$$$$/ $$ \_____   $$ |/  |       
$$ |     $$ |      $$    $$/    $$ |$$       |$$       |  $$  $$/        
$$/      $$/        $$$$$$/__   $$ | $$$$$$$/  $$$$$$$/    $$$$/         
                          /  \__$$ |                                     
                          $$    $$/                                      
                           $$$$$$/                                       
                        
2018

v0.6.11

Resume :
Dans un monde futuriste, Elijah Baley un inspecteur de police et son compagnon R. Daneel Olivaw, un robot humanoïde très sophistiqué investiguent sur une curieuse affaire de meurtre.
Après des mois de recherche, ils finissent par mettre la main sur un suspect. En dépit des preuves accablantes, un problème demeure, le suspect est un robot.
Le problème vous l’aurez compris est que les trois lois de la robotique sont claires : un robot ne peut faire de mal à un être humain.
De longues recherches permettent au duo de découvrir que le meurtre est dû à un dysfonctionnement du cerveau positronique du robot R. Sathan Reventlov.
Devant une audience plénière de l’assemblée Galactique du Sénat de la République, Elijah et R. Daneel doivent démontrer que des processus en conflit au sein d’un cerveau positronique peuvent amener à une rupture des trois lois.
Ils font donc appel à vous, scientifiques reconnus, pour mettre en place le prototype de démonstration.
La démonstration sera composée d’un cerveau positronique représenté par un serveur multi-utilisateur et des processus en conflits représentés par des clients.

Le serveur doit impérativement être développé en C. Il doit être non bloquant et multi-utilisateurs et utiliser zeromq pour ses sockets.
Client fait en python.